const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)

db.defaults({
        reader: {
            index: 0
        },
        all: [],
        valid: [],
    })
    .write()
module.exports = db;