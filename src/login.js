const helpers = require('./helpers');
const db = require('./db');
const signale = require('signale');
const login = async ({
    username,
    password,
    proxy = false
}, index) => {
    signale.info('Index:', index);
    db.set('reader.index', +index).write();
    let form = await helpers.formOptions({
        username,
        password
    });
    return helpers.submitForm(form);
}
module.exports = login;