const login = require('./src/login');
const {
    readUsers
} = require('./src/helpers');
(async () => {
    const {
        users,
        index
    } = readUsers();
    for (user in users) {
        await login(users[user], +user + index);
    }
})();